# Afrikaans texts for GpsPrune #

The following tables show the Afrikaans texts for GpsPrune. There are four columns to each table, firstly the key of each text (used internally by the program), secondly a description of what the text means or when it is displayed. Thirdly the English text which is displayed by default, and fourthly the translation, if any. Please **only edit this fourth column**. The other columns should remain there for reference.

## Menu entries ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
menu.file | Menu for file operations | File | Lęer
menu.file.addphotos |  | Add photos | Voeg Fotos By
menu.file.recentfiles | Submenu for list of recently-used files | Recent files | Onlangse lęers
menu.file.save | Save as text file (not KML or GPX) | Save as text | Stoor
menu.file.exit |  | Exit | Verlaat
menu.online | Online menu, for functions which download from or upload to the internet | Online | Aanlyn
menu.track | Track menu | Track | Baan
menu.track.undo |  | Undo | Herroep
menu.track.clearundo | Clear the list of operations to undo | Clear undo list | Maak herroep lys skoon
menu.track.markrectangle | Allow you to drag a rectangle and mark all the points inside it | Mark points in rectangle | Merk punte
menu.track.deletemarked | Delete the points which have been marked by compression or rectangle | Delete marked points | Vee gemerkde punte uit
menu.range | Range menu | Range | Reeks
menu.range.all | Select all points | Select all | Selekteer alles
menu.range.none | Select no points | Select none | Selekteer niks
menu.range.start | Set the start of the range selection to the current point select from this point onwards | Set range start | Stel reeks begin
menu.range.end | Set the end of the range selection to the current point select up to this point | Set range end | Stel reeks einde
menu.range.average | Make a new point which has the average position of the selection | Average selection | Gemiddelde seleksie
menu.range.reverse | Reverse the order of points within the selected range, so that ABCDE becomes EDCBA | Reverse range | Keer reeks om
menu.range.mergetracksegments | Make the selection a single track segment | Merge track segments | Voeg baan segmente saam
menu.range.cutandmove | Move the selected range to before the currently selected point | Cut and move selection | Knip en skuif seleksie
menu.point | Point menu | Point | Punt
menu.point.editpoint |  | Edit point | Redigeer punt
menu.point.deletepoint |  | Delete point | Punt uitvee
menu.photo | Menu for photo operations | Photo | Foto
menu.photo.saveexif | Save coordinate data to Exif of jpeg files | Save to Exif | Stoor na Exif
menu.audio | Menu for audio operations | Audio | Klank
menu.view | Menu for View operations | View | Bekyk
menu.view.showsidebars | Show or hide the left, right and bottom panels | Show sidebars | Wys rolstaaf
menu.view.browser | Open a browser with a map of the area (not inside GpsPrune window) | Map in a browser window | Kaart in werf blaaier
menu.view.browser.google |  | Google maps | Google kaarte
menu.view.browser.openstreetmap |  | Openstreetmap | Openstreetmap
menu.view.browser.mapquest |  | Mapquest | Mapquest
menu.view.browser.yahoo |  | Yahoo maps | Yahoo Kaarte
menu.view.browser.bing |  | Bing maps | Bing Kaarte
menu.settings | Settings menu | Settings | Stellings
menu.settings.onlinemode | Checkbox for online / offline mode | Load maps from internet | Laai kaarte vanaf internet
menu.settings.autosave | Automatically save settings when exiting program | Autosave settings on exit | Autostoor stellings op uitgaan
menu.help | Help menu | Help | Hulp

## Popup menu for map ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
menu.map.zoomin | zoom in, make everything bigger | Zoom in | Zoem in
menu.map.zoomout | zoom out, make everything smaller | Zoom out | zoem uit
menu.map.zoomfull | zoom so that all points can be seen | Zoom to full scale | Zoem na volskaal
menu.map.newpoint | Create new point at the click position | Create new point | Skep nuwe punt
menu.map.drawpoints | When this is activated, each left-click will create a new point at that position | Create series of points | Skep reeks van punte
menu.map.connect | Connect track points with a line | Connect track points | Koppel baan punte
menu.map.autopan | when a point is selected out of view, automatically pan the view so that the point can be seen | Autopan | Skuif kyk venster automaties
menu.map.showmap | Show map from openstreetmap behind track | Show map | Wys kaart
menu.map.showscalebar | Show scalebar (km or miles) | Show scalebar | Wys skalleerstaaf
menu.map.editmode | When activated, points can be dragged | Edit mode | Wysigingsmode

## Alt keys for menus ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
altkey.menu.file | The accelerator (with Alt) to open each menu | F | L
altkey.menu.online |  | N | 
altkey.menu.track |  | T | B
altkey.menu.range |  | R | R
altkey.menu.point |  | P | P
altkey.menu.view |  | V | K
altkey.menu.photo |  | O | F
altkey.menu.audio |  | A | A
altkey.menu.settings |  | S | S
altkey.menu.help |  | H | H

## Ctrl shortcuts for menu items ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
shortcut.menu.file.open | The shortcut (with Ctrl) to activate each menu item | O | O
shortcut.menu.file.load | (from GPS) | L | L
shortcut.menu.file.save |  | S | S
shortcut.menu.track.undo |  | Z | Z
shortcut.menu.edit.compress |  | C | C
shortcut.menu.range.all |  | A | A
shortcut.menu.help.help |  | H | H

## Functions ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
function.open |  | Open file | Open lęer
function.importwithgpsbabel |  | Import file with GPSBabel | Voer GPSBabel lęer in
function.loadfromgps |  | Load data from GPS | Laai data vanaf GPS
function.sendtogps |  | Send data to GPS | Stuur data na GPS
function.exportkml | Export data | Export KML | Voer uit na KML
function.exportgpx | Export data | Export GPX | Voer uit na GPX
function.exportpov | Export file for 3d rendering | Export POV | Voer uit na POV
function.exportsvg | Export 3d view as vector picture | Export SVG | Voer uit na SVG
function.exportimage | Export map image | Export image | Voer uit na beeld
function.editwaypointname |  | Edit waypoint name | Redigeer baken naam
function.compress | Start the compression algorithms | Compress track | Kompakteer baan
function.deleterange | Delete the currently selected range | Delete range | Vee reeks uit
function.croptrack | Crop the track to just contain the current selection | Crop track | Kleinknip baan
function.interpolate | Insert new points between each pair of points, equally spaced along straight lines | Interpolate points | Interpoleer punte
function.deletebydate | List the dates from the track and select which data to keep and which to delete | Delete points by date | Vee punte uit volgens datum
function.addtimeoffset | Add a specified time offset to the selected range | Add time offset | Voeg tyd spruit by
function.addaltitudeoffset | Add a specified altitude offset to the selected range | Add altitude offset | Voeg hoogte spruit by
function.rearrangewaypoints | Rearrange the order of waypoints within the point series | Rearrange waypoints | Herrangskik bakens
function.convertnamestotimes | Try to interpret waypoint names as timestamps | Convert waypoint names to times | Skakel baken name oor na tyd stempels
function.deletefieldvalues | Delete the values of a specified field | Delete field values | Verwyder veld waardes
function.findwaypoint | Search for waypoints with given name | Find waypoint | Vind baken
function.pastecoordinates | Paste coordinates from wikipedia, geocaching sites etc | Enter new coordinates | Verskaf nuwe koordinate
function.charts |  | Charts | Grafieke
function.show3d | Show a new window with a 3d view of the data | Three-D view | Vertoon 3D
function.distances | Show a list of distances between waypoints | Distances | Afstande
function.fullrangedetails | Show the additional range details like pace, gradient | Full range details | Vol reeks besonderhede
function.estimatetime | Use typical speeds to estimate how long it would take to cover the current range | Estimate time | Skat tyd
function.learnestimationparams | Use the times in the current track to calculate how to estimate times | Learn time estimation parameters | Leer tyd skatings paramters
function.setmapbg | Set the tile server for the background maps | Set map background | Stel kaart agtergrond
function.setpaths | Set the paths to the external programs like gpsbabel | Set program paths | Stel program paaie
function.selectsegment | Select the current segment | Select current segment | Selekteer huidige segment
function.splitsegments | Split the track into segments | Split track into segments | Verdeel baan in segmente
function.sewsegments | Shuffle and reverse the track segments as necessary to join them together | Sew track segments together | Naai baan segmente aanmekaar
function.getgpsies | Get a list of tracks from gpsies.com from the current area | Get Gpsies tracks | Kry Spsies spore
function.uploadgpsies | Send the current data to gpsies.com | Upload track to Gpsies | Laai baan op na Gpsies
function.lookupsrtm | Lookup the point coordinates in SRTM data to find (approx) altitudes | Get altitudes from SRTM | Kry hoogtes vanaf SRTM
function.downloadsrtm | Download the height data from SRTM and save it in the disk cache | Download SRTM tiles | Laai SRTM teëls af
function.getwikipedia | Lookup wikipedia articles about places nearby | Get nearby Wikipedia articles | Kry nabye Wikipedia artikels
function.searchwikipedianames | Lookup Wikipedia articles by name | Search Wikipedia by name | Soek Wikiepdia volgens naam
function.searchopencachingde | Search for geocaches near the current point | Search OpenCaching.de | Soek OpenCaching.de
function.downloadosm | Download the (large) OSM raw data file for the area covered by the current track | Download OSM data for area | Laai OSM data vir area af
function.duplicatepoint | Copy the current point to the end of the track | Duplicate point | Dupliseer punt
function.setcolours | Specify the colours used for points, selections, backgrounds | Set colours | Stel kleure
function.setlinewidth | Set the line width used for drawing tracks | Set line width | Stel lyn dikte
function.setlanguage | Set the language to be used (requires restart) | Set language | Stel tale
function.connecttopoint | Connect current photo (or audio clip) to current point | Connect to point | Koppel na punt
function.disconnectfrompoint | Disconnect photo (or audio clip) from point | Disconnect from point | Ontkoppel van huidige punt
function.removephoto | Remove photo from list, don't delete the file! | Remove photo | Verwyder foto
function.correlatephotos | Correlate all photos using timestamps | Correlate photos | Korreleer fotos
function.rearrangephotos | Rearrange photo points within the track | Rearrange photos | Herrangskik fotos
function.rotatephotoleft | Rotate current photo 90 degrees anticlockwise | Rotate photo left | Roteer foto links
function.rotatephotoright | Rotate current photo 90 degrees clockwise | Rotate photo right | Roteer foto regs
function.photopopup | Open a new window showing the photo bigger | Show photo popup | Vertoon foto 
function.ignoreexifthumb | Don't use the exif thumbnail, load the whole image | Ignore exif thumbnail | Ignoreer Exif thumbnail
function.loadaudio |  | Add audio clips | Voeg klank insetsels by
function.removeaudio | Remove the current audio clip from the list | Remove audio clip | Verwyder klank insetsels
function.correlateaudios | Correlate all audio clips using timestamps | Correlate audios | Korreleer klankgrepe
function.playaudio |  | Play audio clip | Speel klankgrepe
function.stopaudio | Stop playing the current audio clip | Stop audio clip | Stop klankgreep terugspeel
function.help | Show a help message | Help | Hulp
function.showkeys | Show a list of shortcut keys | Show shortcut keys | Wys kortpad sleutels
function.about | Show the "About" screen | About GpsPrune | Omtrent GpsPrune
function.checkversion | Check if a new version of GpsPrune has been released | Check for new version | Kyk vir nuwe weergawe
function.saveconfig | Save the configuration settings like directories, GPS device | Save settings | Stoor stellings
function.diskcache | Settings for disk caching of map images | Save maps to disk | Stoor kaarte na skyf
function.managetilecache | Controls for deleting old map tiles | Manage tile cache | Bestuur teël stoor
function.getweatherforecast | Download a weather forecast for the current area | Get weather forecast | Kry weer voorspelling
function.setaltitudetolerance | Set the altitude variation which is ignored for climb and descent values | Set altitude tolerance | Stel hoogte tolleransie

## Dialogs ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
dialog.exit.confirm.title |  | Exit GpsPrune | Gaan uit GpsPrune
dialog.exit.confirm.text |  | Your data is not saved. Are you sure you want to exit? | Jou data is nie gestoor nie. Is jy seker jy wil uitgaan?
dialog.openappend.title |  | Append to existing data | Voegby tot bestaande data
dialog.openappend.text | Append means if I open file1 and then file2, I have both data together. If I say no, I remove file1 data and just have file2. | Append this data to the data already loaded? | Voeg hierdie data by die data wat alreeds gelaai is?
dialog.deletepoint.title |  | Delete Point | Vee punt uit
dialog.deletepoint.deletephoto | Question whether to remove photo from list or just disconnect from point | Delete photo attached to this point? | Vee aangehegde foto van die punt uit?
dialog.deletephoto.title |  | Delete Photo | Vee foto uit
dialog.deletephoto.deletepoint | Question whether to delete the point attached to the photo being removed | Delete point attached to this photo? | Vee punt uit wat aan hierdie foto geheg is?
dialog.deleteaudio.deletepoint | Same as above but for an audio clip being removed | Delete point attached to this audio clip? | Vee punt uit wat aan hierdie klankgreep geheg is?
dialog.openoptions.title |  | Open options | Maak opsies oop
dialog.openoptions.filesnippet | label for showing a small part of the file | Extract of file | Uittreksel vanuit Lęer
dialog.load.table.field |  | Field | Veld
dialog.load.table.datatype |  | Data Type | Datatipe
dialog.load.table.description |  | Description | Beskrywing
dialog.delimiter.label | The character which separates fields of data | Field delimiter | Veld skeidings karakter
dialog.delimiter.comma |  | Comma , | Komma ,
dialog.delimiter.tab |  | Tab | Oortjie
dialog.delimiter.space |  | Space | Spasie
dialog.delimiter.semicolon |  | Semicolon ; | Kommapunt ;
dialog.delimiter.other |  | Other | Ander
dialog.openoptions.deliminfo.records |  | records, with | Rekords, met
dialog.openoptions.deliminfo.fields | These two make the message "&lt;number&gt; records, with &lt;number&gt; fields" to describe what was found | fields | velde
dialog.openoptions.deliminfo.norecords |  | No records | Geen rekords
dialog.openoptions.altitudeunits |  | Altitude units | Hoogte eenhede
dialog.openoptions.speedunits |  | Speed units | Spoed eenhede
dialog.openoptions.vertspeedunits |  | Vertical speed units | Vertikale eenhede
dialog.openoptions.vspeed.positiveup | Radio buttons for meaning of positive vertical speed values | Positive speeds upwards | Positiewe spoed opwaarts
dialog.openoptions.vspeed.positivedown |  | Positive speeds downwards | Positiewe spoed afwaarts
dialog.open.contentsdoubled | Warning message shown when all points are given twice | This file contains two copies of each point,\nonce as waypoints and once as track points. | Hierdie leer bevat twee kopië van elke punt,\n eenkeer as baken en eenkeer as baan punt
dialog.selecttracks.intro | Message for selecting tracks when loading a gpx file | Select the track or tracks to load | Selekteer die baan of bane om te laai
dialog.selecttracks.noname | Displayed for tracks without a name | Unnamed | naamlose
dialog.jpegload.subdirectories | Option to search through all subdirectories as well | Include subdirectories | Sluit sub-gidse in
dialog.jpegload.loadjpegswithoutcoords | Option to also load jpegs which don't have coordinates in them | Include photos without coordinates | Sluit fotos sonder koordinate in
dialog.jpegload.loadjpegsoutsidearea | Option to also load jpegs which are outside the current track area | Include photos outside current area | Sluit foto buitekant huidige area in
dialog.jpegload.progress.title |  | Loading photos | Fotos laai
dialog.jpegload.progress |  | Please wait while the photos are searched | Wag asseblief terwyl na fotos gesoek word
dialog.gpsload.nogpsbabel |  | No gpsbabel program could be found. Continue? | Geen gpsbabel program kon gevind word. Gaanvoort?
dialog.gpsload.device | parameter for gpsbabel, eg '/dev/ttyUSB0' | Device name | Apparaat naam
dialog.gpsload.format | parameter for gpsbabel, eg 'garmin' | Format | Formaat
dialog.gpsload.getwaypoints |  | Load waypoints | Laai bakens
dialog.gpsload.gettracks | Two checkboxes for what to load from GPS | Load tracks | Laai bane
dialog.gpsload.save | Checkbox to save GPS data immediately to file | Save to file | Stoor na lęer
dialog.gpssend.sendwaypoints |  | Send waypoints | Stuur bakens
dialog.gpssend.sendtracks | Two checkboxes for what to send to GPS | Send tracks | Stuur bane
dialog.gpssend.trackname | Name of track as it appears in GPS unit | Track name | Baan name
dialog.gpsbabel.filters | Area of dialog dealing with GPSBabel filters | Filters | Filters
dialog.addfilter.title | Dialog title for adding a new GPSBabel filter | Add filter | Voeg filter by
dialog.gpsbabel.filter.discard | Four buttons for the types of GPSBabel filter | Discard | Weggooi
dialog.gpsbabel.filter.simplify |  | Simplify | Vereenvoudig
dialog.gpsbabel.filter.distance |  | Distance | Afstand
dialog.gpsbabel.filter.interpolate |  | Interpolate | Interpoleer
dialog.gpsbabel.filter.discard.intro | Introduction to discard filter settings | Discard points if | Weggooi van punte as
dialog.gpsbabel.filter.discard.hdop | Horizontal dilution of precision | Hdop > | Hdop >
dialog.gpsbabel.filter.discard.vdop | Vertical dilution of precision | Vdop > | Vdop >
dialog.gpsbabel.filter.discard.numsats | Number of satellites | Number of satellites < | Aantal satelliete
dialog.gpsbabel.filter.discard.nofix | Checkbox for discard filter | Point has no fix | Punt het geen binding
dialog.gpsbabel.filter.discard.unknownfix | Checkbox for discard filter | Point has unknown fix | Punt het unbekende binding
dialog.gpsbabel.filter.simplify.intro | Introduction to simplify filter settings | Remove points until | Verwyder punte totdat
dialog.gpsbabel.filter.simplify.maxpoints | Label before max points box | Number of points < | Aantal punte
dialog.gpsbabel.filter.simplify.maxerror | Label before max error box | or error distance < | of fout afstand <
dialog.gpsbabel.filter.simplify.crosstrack | Three radio buttons for error type | cross-track | kruis-baan
dialog.gpsbabel.filter.simplify.length |  | length difference | lengte verskil
dialog.gpsbabel.filter.simplify.relative |  | relative to hdop | relatief to hdop
dialog.gpsbabel.filter.distance.intro | Introduction to distance filter settings | Remove points if close to any previous point | Verwyder punt as naby aan vorige punt
dialog.gpsbabel.filter.distance.distance | Label before distance box | If distance < | As afstand <
dialog.gpsbabel.filter.distance.time | Label before time limit box | and time difference < | en tyd verskil <
dialog.gpsbabel.filter.interpolate.intro | Introduction to interpolate settings | Add extra points between track points | Voeg ekstra punte tussen baan punte
dialog.gpsbabel.filter.interpolate.distance | Label before distance box | If distance > | As afstand >
dialog.gpsbabel.filter.interpolate.time | Label before time box | or time difference > | of tyd verskil >
dialog.saveoptions.title |  | Save file | Stoor lęer
dialog.save.fieldstosave | Label for the table of which fields to save to the file | Fields to save | Velde om te stoor
dialog.save.table.field |  | Field | Veld
dialog.save.table.hasdata | A checkbox which is checked if the field has data in it | Has data | Het data
dialog.save.table.save |  | Save | Stoor
dialog.save.headerrow | A checkbox which can be checked to also save a header row | Output header row | Stoor opskrif ry
dialog.save.coordinateunits |  | Coordinate format | Koordinaat formaat
dialog.save.altitudeunits |  | Altitude units | Hoogte eenhede
dialog.save.timestampformat |  | Timestamp format | Tyd stempel formaat
dialog.save.overwrite.title |  | File already exists | Lęer bestaan alreeds
dialog.save.overwrite.text |  | This file already exists. Are you sure you want to overwrite the file? | Hierdie lęer bestaan reeds. Is jy seker jy wil die lęer oorskryf?
dialog.save.notypesselected | At least one of the three point types (track, way, photo) should be selected for any save/export | No point types have been selected | Geen punt tipes is geselekteer nie
dialog.exportkml.text | Title text saved to file | Title for the data | Titel vir die data
dialog.exportkml.altitude | altitudes are always included in KML but this checkbox specifies "absolute", not "clampToGround" | Absolute altitudes (for aviation) | Absolute hoogte (vir lugvaart)
dialog.exportkml.kmz | Checkbox for export to kmz or kml | Compress to make kmz file | Kompakteer om kmz lęer te maak
dialog.exportkml.exportimages | Checkbox to also save thumbnails of photos | Export image thumbnails to kmz | Voer beeld duimnaelsketse uit na kmz
dialog.exportkml.imagesize | Size of the image thumbnails in the KMZ (pixels) | Image size | Beeld groote
dialog.exportkml.trackcolour | Label for colour patch for selecting colour of track | Track colour | Baan kleur
dialog.exportkml.standardkml | Two radio buttons - this one is plain KML as before, without timestamps | Standard KML | Standaard KML
dialog.exportkml.extendedkml | Second radio button - this one uses Google's extensions to KML allowing timestamps (if present) | Extended KML with timestamps | Uitgebreide KML met tydstempels
dialog.exportgpx.name |  | Name | Naam
dialog.exportgpx.desc |  | Description | Beskrywing
dialog.exportgpx.includetimestamps | Checkbox to include timestamps in GPX file | Include timestamps | Sluit tydstempels in
dialog.exportgpx.copysource | Checkbox to copy loaded xml if possible | Copy source xml | Kopieër bron xml
dialog.exportgpx.encoding | Panel for selecting character encoding | Encoding | Kodering
dialog.exportgpx.encoding.system | The default system encoding | System | Stelsel
dialog.exportgpx.encoding.utf8 | Radio button to use UTF-8, ignoring system settings | UTF-8 | UTF-8
dialog.exportpov.text |  | Please enter the parameters for the POV export | Steutel asseblief parameters in vir POV uitvoer
dialog.exportpov.font |  | Font | Font
dialog.exportpov.camerax | X coordinate of camera | Camera X | Kamera X
dialog.exportpov.cameray | Y coordinate of camera | Camera Y | Kamera Y
dialog.exportpov.cameraz | Z coordinate of camera | Camera Z | Kamera Z
dialog.exportpov.modelstyle | Style of model to use | Model style | Model styl
dialog.exportpov.ballsandsticks | 'old method' where each point is a ball on a stick | Balls and sticks | Balle en stokkies
dialog.exportpov.tubesandwalls | 'new method' where the track looks like a tube of toothpaste | Tubes and walls | Buise en mure
dialog.3d.warningtracksize | Warning about large track size, asking whether to continue or not | This track has a large number of points, which Java3D might not be able to display.\nAre you sure you want to continue? | Hierdie baan het 'n groot aantal punte, wat Java3D miskien nie kan vertoon.\nIs jy seker jy wil voortgaan?
dialog.3d.useterrain | Checkbox for drawing a 3d terrain or not | Show terrain | Wys terrein
dialog.3d.terraingridsize | Edit box for entering the number of points to use in the terrain grid | Grid size | Rooster groote
dialog.exportpov.baseimage | Label for image used on the base plane of the pov export | Base image | Basis beeld
dialog.exportpov.cannotmakebaseimage | Error if base image can't be saved for some reason | Cannot write base image | Kan nie basis beeld skryf
dialog.baseimage.title | Title of popup window | Map image | Kaart beeld
dialog.baseimage.useimage | Checkbox for using an image or not | Use image | Gebruik beeld
dialog.baseimage.mapsource |  | Map source | Kaar bron
dialog.baseimage.zoom |  | Zoom level | Vergrotings vlak
dialog.baseimage.incomplete | Orange label shown when not all the map tiles were found | Image incomplete | Beeld onvolledig
dialog.baseimage.tiles | Number of tiles used and total (like 7 / 7) | Tiles | Teëls
dialog.baseimage.size | Width and height of image in pixels | Image size | Beeld groote
dialog.exportsvg.text | Introduction text to export svg dialog | Select the parameters for the SVG export | Selekteer die parameters vir die SVG uitvoer
dialog.exportsvg.phi | Azimuth angle of viewpoint (0 from South up to 360) | Azimuth angle ? | Azimuth hoek ?
dialog.exportsvg.theta | Elevation angle of viewpoint (0 means base, 90 means overhead) | Elevation angle ? | Opstandings angle ?
dialog.exportsvg.gradients | Checkbox to use gradients or flat colours for point fills | Use gradients for shading | Grbruik gradient vir skakering
dialog.exportimage.noimagepossible | Error if disk cache isn't being used | Map images need to be cached to disk in order to use them for an export. | Kaart beelde moet gestoor word na skuif sodat dit gebruik kan word vir uitvoer
dialog.exportimage.drawtrack | Checkbox to draw the track on top of the exported map image | Draw track on map | Teken baan op kaart
dialog.exportimage.drawtrackpoints | Checkbox to also draw each track point or just the lines between | Draw track points | Teken baan punte
dialog.exportimage.textscalepercent | Percentage scale factor for the font size (100 means normal font size) | Text scale factor (%) | Teks skaleer faktor (%)
dialog.pointtype.desc | Describe the meaning of these checkboxes | Save the following point types: | Stoor die volgende punt tipes
dialog.pointtype.track | Save track points | Track points | Baan punte
dialog.pointtype.waypoint | Save waypoints | Waypoints | Bakens
dialog.pointtype.photo | Save photo points | Photo points | Foto punte
dialog.pointtype.audio | Save points with audio clips attached | Audio points | Klank punte
dialog.pointtype.selection | Checkbox to just save the currently selected range | Just selection | Net seleksie
dialog.confirmreversetrack.title | Title of dialog to confirm whether to really do a section reversal or not | Confirm reversal | Bevestig omkering
dialog.confirmreversetrack.text | This message just makes sure it is ok to do a reversal even though the data contains timestamps. Assuming that the timestamps are in time order, doing a reversal will put the timestamps out of sequence. | This track contains timestamp information, which will be out of sequence after a reversal.\nAre you sure you want to reverse this section? | Hierdie baan bevat tydstempel informasie, wat uit sekwensie/order sal wees na omkering.\nIs jy seker jy wil die baan omruil vir die seksie?
dialog.confirmcutandmove.title |  | Confirm cut and move | Bevestig knip en skuif
dialog.confirmcutandmove.text | Same as above but for moving a track section | This track contains timestamp information, which will be out of sequence after a move.\nAre you sure you want to move this section? | Hierdie baan het tydstempel informasie, wat uit sekwensie/orde sal wees na skuif.\nIs jy seker jy wil die seksie skuif?
dialog.interpolate.parameter.text | Number of points to insert between each pair of points in the selected range | Number of points to insert between each pair of points | Aantal punte om bytevoeg tussen geselekteerde punte.
dialog.interpolate.betweenwaypoints | Question whether to create new track points between each pair of waypoints | Interpolate between waypoints? | Iterpoleer tussen bakens?
dialog.undo.title |  | Undo action(s) | Herroep aksie(s)
dialog.undo.pretext |  | Please select the action(s) to undo | Selekteer asb die aksie(s) om te herroep
dialog.undo.none.title |  | Cannot undo | Kan nie herroep
dialog.undo.none.text |  | No operations to undo! | Geen operasies om te herroep!
dialog.clearundo.title |  | Clear undo list | Maak herroep lys skoon
dialog.clearundo.text |  | Are you sure you want to clear the undo list?\nAll undo information will be lost! | Is jy seker jy wil die herroep lys skoon maak?\nAlle herroep informasie sal verlore gaan!
dialog.pointedit.title |  | Edit point | Redigeer punt
dialog.pointedit.intro | Description at the top of the edit point dialog | Select each field in turn to view and change the value | Selekteer elke veld om die waarde te verander
dialog.pointedit.table.field |  | Field | Veld
dialog.pointedit.nofield | for label on right-hand side of point edit dialog | No field selected | 
dialog.pointedit.table.value |  | Value | Waarde
dialog.pointnameedit.name |  | Waypoint name | Baken naam
dialog.pointnameedit.uppercase |  | UPPER case | Hoofletter
dialog.pointnameedit.lowercase |  | lower case | Kleinletter
dialog.pointnameedit.titlecase | capitalise each word of the name | Title Case | Titel Kas
dialog.addtimeoffset.add | Make points later | Add time | Tel tyd by
dialog.addtimeoffset.subtract | Make points earlier | Subtract time | Trek tyd af
dialog.addtimeoffset.days |  | Days | Dae
dialog.addtimeoffset.hours |  | Hours | Ure
dialog.addtimeoffset.minutes |  | Minutes | Minute
dialog.addtimeoffset.notimestamps | Error message shown when no points with timestamps are selected | Cannot add a time offset as this selection doesn't contain any timestamp information | Kan nie tyd offset by tel want die seleksie bevat nie enige tydstempel informasie nie
dialog.findwaypoint.intro | Introductory text for find waypoint dialog | Enter part of the waypoint name | Sleutel gedeelte van baken naam
dialog.findwaypoint.search | Label for search box | Search | Soek
dialog.saveexif.title | Title for save exif dialog | Save Exif | Stoor Exif
dialog.saveexif.intro |  | Select the photos to save using the checkboxes | Selekter die fotos om te stoor deur gebruik te maak van merkblokkie
dialog.saveexif.nothingtosave | Message when nothing to do | Coordinate data is unchanged, nothing to save | Koordinaat data is onveranderd, niks om te stoor
dialog.saveexif.noexiftool | Message when exiftool not found | No exiftool program could be found. Continue? | Geen exiftool program kon gevind word. Gaan voort?
dialog.saveexif.table.photoname |  | Photo name | Foto naam
dialog.saveexif.table.status | Column heading for status (see values below) | Status | Status
dialog.saveexif.table.save | Column heading for save checkbox | Save | Stoor
dialog.saveexif.photostatus.connected | Photo had no point, now it has | Connected | Verbind
dialog.saveexif.photostatus.disconnected | Photo did have a point, now it doesn't | Disconnected | Onkoppel
dialog.saveexif.photostatus.modified | Photo did have a point but it's been edited | Modified | Verander
dialog.saveexif.overwrite | Check to overwrite jpg files | Overwrite files | Oorskruif leęrs
dialog.saveexif.force | Add flag to ignore minor errors and force exif write | Force despite minor errors | Vorseer ten spyte van klein foute
dialog.charts.xaxis | Panel to choose x axis for charts | X axis | X as
dialog.charts.yaxis | Panel to choose y axes for charts | Y axes | Y as
dialog.charts.output | Panel to choose where to output to | Output | Uitset
dialog.charts.screen |  | Output to screen | Uitset na skerm
dialog.charts.svg |  | Output to SVG file | Uitset na SVG leer
dialog.charts.svgwidth | width and height of SVG file in pixels | SVG width | SVG wydte
dialog.charts.svgheight |  | SVG height | SVG hoogte
dialog.charts.needaltitudeortimes | Without altitudes and without times, can't draw any charts | The track must have either altitudes or time information in order to create charts | die baan moet of hoogtes of tyd informasie bevat om grafiek teskep
dialog.charts.gnuplotnotfound | Error message shown when gnuplot path changed but still not found | Could not find gnuplot with the given path | Kon nie gnuplot find op gegewe pad
dialog.distances.intro |  | Straight line distances between points | Reguit lyn afstande tussen punte
dialog.distances.column.from | Column heading for distances dialog | From point | Vanaf punt
dialog.distances.column.to |  | To point | Na punt
dialog.distances.currentpoint | Table entry for current track point | Current point | Huidige punt
dialog.distances.toofewpoints | Message shown when less than two waypoints available | This function needs waypoints in order to calculate the distances between them | Hierdie funksie benodig bakens om afstande tussen bakens uit tewerk
dialog.fullrangedetails.intro | Message at top of "full range details" dialog | Here are the details for the selected range | Hier is die besonderhede for die geselekteerde reeks
dialog.fullrangedetails.coltotal | Column heading for total values | Including gaps | Insluitend gapings
dialog.fullrangedetails.colsegments | Column heading when gaps between segments are ignored | Without gaps | Sonder gapings
dialog.estimatetime.details | Heading for the details section of estimating time | Details | Besonderhede
dialog.estimatetime.gentle | Column heading for low/shallow gradient | Gentle | Delikaat
dialog.estimatetime.steep | Column heading for steep/high gradient | Steep | Styl
dialog.estimatetime.climb |  | Climb | Klim
dialog.estimatetime.descent |  | Descent | Val
dialog.estimatetime.parameters | Heading for parameters section | Parameters | Paramters
dialog.estimatetime.parameters.timefor | Minutes required for travelling a specified distance | Time for | Tyd vir
dialog.estimatetime.results | Heading for results section | Results | Resultate
dialog.estimatetime.results.estimatedtime | Results of calculation in hours, mins and secs | Estimated time | Beraamde tyd
dialog.estimatetime.results.actualtime | Actual required time according to the timestamps | Actual time | Werklike tyd
dialog.estimatetime.error.nodistance | Error shown if current selection only has waypoints or singletons, so moving distance is zero | The time estimates need connected track points, to give a distance | Die beraamings tye het gekoppelde baan punte nodig, om 'n afstand te verskaf
dialog.estimatetime.error.noaltitudes | Warning shown if the current selection hasn't got any altitude information | The selection doesn't include any altitude information | Die seleksie het geen hoogste informasie nie
dialog.learnestimationparams.intro | Label before the results of the parameter-learning | These are the parameters calculated from this track | Hierdie is die parameters wat uitgewerk is vanaf hierdie baan
dialog.learnestimationparams.averageerror | Label for the average percentage error generated by these parameters | Average error | Gemiddelde fout
dialog.learnestimationparams.combine | Label between the calculated results and the combined results | These parameters can be combined with the current values | Hierdie parameters kan gekombineer word met die huidige waardes
dialog.learnestimationparams.combinedresults | Heading for combined results section | Combined results | Gekombineerde resulate
dialog.learnestimationparams.weight.100pccurrent | Slider all the way to the left | Keep current values | Hou huidige waardes
dialog.learnestimationparams.weight.current | used to build the strings eg 60% current + 40% calculated | current | huidig
dialog.learnestimationparams.weight.calculated | used to build the strings eg 60% current + 40% calculated | calculated | bereken 
dialog.learnestimationparams.weight.50pc | Average of current values and calculated ones | Average of current values and calculated ones | Gemiddeld van huidige waardes en berekende waardes
dialog.learnestimationparams.weight.100pccalculated | Slider all the way to the right | Use new calculated values | Gebruik nuwe berekende waardes
dialog.setmapbg.intro | Label at top of map background dialog | Select one of the map sources, or add a new one | Selekteer een van die kaart bronne, of voeg nuwe een by
dialog.addmapsource.title | Title of dialog to add a new map source | Add new map source | Voeg nuwe kaart bron by
dialog.addmapsource.sourcename | Label for source name | Name of source | Naam van bron
dialog.addmapsource.layer1url | Label for text box for URL | URL of first layer | URL van eerste laag
dialog.addmapsource.layer2url | Label for text box for URL | Optional URL of second layer | Ooptionele URL van tweede laag
dialog.addmapsource.maxzoom | Label for dropdown to select maximum zoom level | Maximum zoom level | Maksimum vergrotings vlak
dialog.addmapsource.noname | Name given to new map source if no name entered | Unnamed | Onbenaamd
dialog.gpsies.column.name | Column headings for table in gpsies dialog | Track name | Baan naam
dialog.gpsies.column.length |  | Length | Lengte
dialog.gpsies.description | Heading for description box | Description | Beskrywing
dialog.gpsies.nodescription | Message displayed when no description found | No description | Geen beskrywing
dialog.gpsies.nonefound | Message when no tracks returned from gpsies | No tracks found | Geen bane gevind
dialog.gpsies.username | Your username for the website gpsies.com | Gpsies username | Gpsies gebruikersnaam
dialog.gpsies.password | The password for the above username | Gpsies password | Spsies wagwoord
dialog.gpsies.keepprivate | Checkbox to select private (only you can see your track) or public | Keep track private | Hou baan privaat
dialog.gpsies.confirmopenpage | Confirm dialog asking whether to launch a browser to show the gpsies.com page | Open the web page for the uploaded track? | Maak web blad oop vir opgelaaide baan
dialog.gpsies.activities | Label introducing activity types | Activity types | Aktiwiteits tipes
dialog.gpsies.activity.trekking | The following are 8 of the activity types defined by gpsies.com | Hiking | Stap
dialog.gpsies.activity.walking |  | Walking | Loop
dialog.gpsies.activity.jogging |  | Running | Hardloop
dialog.gpsies.activity.biking |  | Cycling | Fietsry
dialog.gpsies.activity.motorbiking |  | Motorbiking | Moterfietsry
dialog.gpsies.activity.snowshoe |  | Snowshoeing | 
dialog.gpsies.activity.sailing |  | Sailing | Seiljagwedvaart
dialog.gpsies.activity.skating | Inline skating, roller blading | Skating | Skaats
dialog.wikipedia.column.name | Column heading: page name in Wikipedia | Article name | Artikel naam
dialog.wikipedia.column.distance | Column heading: distance of each point from the centre of view | Distance | Afstand
dialog.wikipedia.nonefound | Message when no points returned from wikipedia | No wikipedia entries found | Geen wikipedia insetsels gevind
dialog.correlate.notimestamps | Message when no timestamp information in points | There are no timestamps in the data points, so there is nothing to correlate with the photos. | Daar is geen tydstempels in die data punte, so daar is niks om te korreleer met die fotos
dialog.correlate.nouncorrelatedphotos | Message when all photos are already correlated | There are no uncorrelated photos.\nAre you sure you want to continue? | Daar is geen ongekorreleerde fotos.\nIs jy seker jy wil voortgaan?
dialog.correlate.nouncorrelatedaudios | Message when all audio clips are already correlated | There are no uncorrelated audios.\nAre you sure you want to continue? | Daar is geen ongekorreleerde klankbane
dialog.correlate.photoselect.intro | Heading above table in first correlate panel | Select one of these correlated photos to use as the time offset | Selekteer een van die gekorreleerde fotos om te gebruik as tyd afset
dialog.correlate.select.photoname |  | Photo name | Foto naam
dialog.correlate.select.timediff |  | Time difference | Tyd verskil
dialog.correlate.select.photolater | Is the photo time later than the point time? | Photo later | Foto later
dialog.correlate.options.intro | Heading above table in second correlate panel | Select the options for automatic correlation | Seleketeer die opsies vir automatiese korrelasie
dialog.correlate.options.offsetpanel | Panel title | Time offset | Tyd verplasing
dialog.correlate.options.offset | Heading for time offset boxes | Offset | Verplasing
dialog.correlate.options.offset.hours | three fields for describing text boxes of offset | hours, | ure,
dialog.correlate.options.offset.minutes |  | minutes and | minute en
dialog.correlate.options.offset.seconds |  | seconds | sekondes
dialog.correlate.options.photolater | radio button for direction of time difference | Photo later than point | Foto later as punt
dialog.correlate.options.pointlaterphoto | radio button | Point later than photo | Punt later as foto
dialog.correlate.options.audiolater | radio button | Audio later than point | Klank later as punt
dialog.correlate.options.pointlateraudio | radio button | Point later than audio | Punt later as klank
dialog.correlate.options.limitspanel |  | Correlation limits | Korrelasie limiete
dialog.correlate.options.notimelimit | radio button | No time limit | Geen tyd limiet
dialog.correlate.options.timelimit | radio button | Time limit | Tyd limiet
dialog.correlate.options.nodistancelimit | radio button | No distance limit | Geen afstand limiet
dialog.correlate.options.distancelimit | radio button | Distance limit | Afstand limiet
dialog.correlate.options.correlate | Table column heading | Correlate | Korreleer
dialog.correlate.alloutsiderange | Message shown when all photos are outside time range of track | All the items are outside the time range of the track, so none can be correlated.\nTry changing the offset or manually correlating at least one item. | Al die fotos is buitekant die tyd reeks van die baan, so geen kon gekorreleer word.\nProbeer om afset te stel of omself ten minste een foto te korreleer.
dialog.correlate.filetimes | First half of explanation message | File timestamps denote: | Leęr tydstempels dui aan:
dialog.correlate.filetimes2 | Second half of text after the three radio buttons | of audio clip | van klank greep
dialog.correlate.correltimes | Explanation of three following radio buttons | For correlation, use: | Vir korrelasie gebruik:
dialog.correlate.timestamp.beginning |  | Beginning | Begin
dialog.correlate.timestamp.middle |  | Middle | Middel
dialog.correlate.timestamp.end |  | End | Einde
dialog.correlate.audioselect.intro | Introduction text to first panel of audio correlation | Select one of these correlated audios to use as the time offset | Selekteer een van dié gekorreleerde klankgrepe om te gebruik as tyd verplasing
dialog.correlate.select.audioname | Table column heading for audio clipname | Audio name | Klankgreep naam
dialog.correlate.select.audiolater | Table column heading | Audio later | Klank later
dialog.rearrangewaypoints.desc | Message for the top of the rearrange waypoints dialog | Select the destination and sort order of the waypoints | Selekteer die eindbestemming en sorteer volgorde van die bakens
dialog.rearrangephotos.desc | Message for the top of the rearrange photos dialog | Select the destination and sort order of the photo points | Selekteer die eindbestemming en sorteer volgorde van die foto punte
dialog.rearrange.tostart |  | Move to start | Beweeg na begin
dialog.rearrange.toend |  | Move to end | Beweeg na einde
dialog.rearrange.tonearest |  | Each to nearest track point | Beweeg elk na naaste baan punt
dialog.rearrange.nosort | Leave points/photos in current order | Don't sort | Nie sorteer
dialog.rearrange.sortbyfilename | Sort by filename of photo | Sort by filename | Sorteer volgens leernaam
dialog.rearrange.sortbyname | Sort by waypoint name | Sort by name | Sorteer volgens naam
dialog.rearrange.sortbytime |  | Sort by time | Sorteer volgens tyd
dialog.compress.closepoints.title | checkbox for turning on removal of points close to other ones | Nearby point removal | Naby punt verwydering
dialog.compress.closepoints.paramdesc | parameter for close point removal | Span factor | Span faktor
dialog.compress.wackypoints.title | checkbox for turning on removal of "wacky" or unusual points | Wacky point removal | Gekkige punt verwydering
dialog.compress.wackypoints.paramdesc | parameter for wacky point removal | Distance factor | Afstands faktor
dialog.compress.singletons.title | checkbox for turning on removal of singletons | Singleton removal | Enkeling verwydering
dialog.compress.singletons.paramdesc | parameter for singleton removal | Distance factor | Afstands faktor
dialog.compress.duplicates.title | checkbox for turning on duplicate removal | Duplicate removal | Duplikaat verwydering
dialog.compress.douglaspeucker.title |  | Douglas-Peucker compression | Douglas-Peuker kompressie
dialog.compress.douglaspeucker.paramdesc |  | Span factor | Span faktor
dialog.compress.summarylabel | label to say how many will be deleted | Points to delete | Punte om te verwyder
dialog.compress.confirm | Makes a message "<number of points> have been marked" and asks whether to delete them now | %d points have been marked.\nDelete these marked points now? | %d punte is gemerk.\Verwyder hierdie punte nou?
dialog.compress.confirmnone |  | no points have been marked | geen punte is gemerk nie
dialog.deletemarked.nonefound | warning message for delete marked points when no points are marked | No data points could be removed | Geen data punte kon verwyder word
dialog.pastecoordinates.desc |  | Enter or paste the coordinates here | Sleutel of plak die koordinate hier
dialog.pastecoordinates.coords | Label for coordinates entry box | Coordinates | Koordinate
dialog.pastecoordinates.nothingfound | Error message when coordinates couldn't be parsed | Please check the coordinates and try again | Gaan asseblief koordinate na en probeer weer
dialog.help.help | Popup message to direct to website | Please see\n http://gpsprune.activityworkshop.net/\nfor more information and tips,\nincluding a new PDF user guide you can buy. | Sien asseblief\n http://gpsprune.activityworkshop.net/\n vir meer inligting en gebruikers handleidings.
dialog.about.version |  | Version | Weergawe
dialog.about.build |  | Build | Bou
dialog.about.summarytext1 |  | GpsPrune is a program for loading, displaying and editing data from GPS receivers. | GpsPrune is 'n program vir die laai, vertoon en wysiging van data vanaf GPS ontvangers.
dialog.about.summarytext2 |  | It is released under the Gnu GPL for free, open, worldwide use and enhancement.<br>Copying, redistribution and modification are permitted and encouraged<br>according to the conditions in the included <code>license.txt</code> file. | Dit is vrygestel onder die Gnu GPL gratif, oop węreldwye gebruik en verbetering.<br>Kopiëring, herverspreiding en veranderings word toegelaat en aangemoedig<br>volgens die kondisies in the aangegte <code>license.txt</code> leęr.
dialog.about.summarytext3 |  | Please see <code style="font-weight:bold">http://activityworkshop.net/</code> for more information and tips, including<br>a new PDF user guide you can buy. | Sien asseblief <code style="font-weight:bold"http://activityworkshop.net/</code> vir mer inligting en wenke, insluitend<br> 'n nuwe PDF gebruikers gids wat jy kan koop.
dialog.about.languages | languages available in GpsPrune | Available languages | Beskikbare tale
dialog.about.translatedby | Name or nickname of translator for this language | English text by activityworkshop. | Engelse teks deur activityworkshop
dialog.about.systeminfo | Tab heading for system information | System info | Stelsel informasie
dialog.about.systeminfo.os |  | Operating System | Beheer Stelsel
dialog.about.systeminfo.java | Version of java | Java Runtime | Java Runtime
dialog.about.systeminfo.java3d |  | Java3d installed | Java3d geinstalleer
dialog.about.systeminfo.povray |  | Povray installed | Povray geinstalleer
dialog.about.systeminfo.exiftool |  | Exiftool installed | Exiftool geinstalleer
dialog.about.systeminfo.gpsbabel |  | Gpsbabel installed | Gpsbabel geinstalleer
dialog.about.systeminfo.gnuplot |  | Gnuplot installed | Gnuplot geinstalleer
dialog.about.systeminfo.exiflib | Library used for reading exif tags | Exif library | Exif biblioteek
dialog.about.systeminfo.exiflib.internal | Included with GpsPrune (normal) | Internal | Intern
dialog.about.systeminfo.exiflib.internal.failed | Should be included but wasn't (won't work) | Internal (not found) | Interne (nie gevind)
dialog.about.systeminfo.exiflib.external | Relying on external jar file | External | Ekstern
dialog.about.systeminfo.exiflib.external.failed | External jar file not found, so won't work | External (not found) | Ekstern (nie gevind)
dialog.about.yes | Shown when item installed | Yes | Ja
dialog.about.no | Shown when item not available | No | Nee
dialog.about.credits | Tab heading for credits (acknowledgements) | Credits | Krediete
dialog.about.credits.code |  | GpsPrune code written by | GpsPrune bron kode geskruif deur
dialog.about.credits.exifcode |  | Exif code by | Exif bron kode deur
dialog.about.credits.icons |  | Some icons taken from | Somigge ikone gevat vanaf
dialog.about.credits.translators | Who did the translations | Translators | Vertalers
dialog.about.credits.translations |  | Translations helped by | Vertalings gehelp deur
dialog.about.credits.devtools |  | Development tools | Onwikkelings gereedskap
dialog.about.credits.othertools |  | Other tools | Ander gereedskap
dialog.about.credits.thanks |  | Thanks to | Dankie aan
dialog.about.readme | Tab for readme file | Readme | Leesmy
dialog.checkversion.error | Error message when file couldn't be found, or online connection not available | The version number couldn't be checked.\nPlease check the internet connection. | Die weergawe nommer kon nie opgespoor word nie.\nGaan asseblief die internet konneksie na.
dialog.checkversion.uptodate | Shown when this is already the latest version | You are using the latest version of GpsPrune. | Jy gebruik die nuuste weergawe van GpsPrune
dialog.checkversion.newversion1 |  | A new version of GpsPrune is now available!  The latest version is now version | 'n nuwe weergawe van GpsPrune is nou beskikbaar! Die nuuste weergawe is nou weergawe
dialog.checkversion.newversion2 | These two make the message to say that a new version <number> is now released | . | .
dialog.checkversion.releasedate1 |  | This new version was released on | Hierdie nuwe weergawe was vrygestel op
dialog.checkversion.releasedate2 | These two make the messsage that the new one was released on <date>. | . | .
dialog.checkversion.download | Where to download the newest version. | To download the new version, go to http://gpsprune.activityworkshop.net/download.html. | Om die nuwe weergawe aftelaai, gaan na http://gpsprune.activityworkshop.net/download.html. 
dialog.keys.intro | Explanation of key shortcuts | You can use the following shortcut keys instead of using the mouse | Jy kan die volgende kortpadsleutels gebruik in plaas van om die muis te gebruik
dialog.keys.keylist | HTML table showing key shortcuts | <table><tr><td>Arrow keys</td><td>Pan map left right, up, down</td></tr><tr><td>Ctrl + left, right arrow</td><td>Select previous or next point</td></tr><tr><td>Ctrl + up, down arrow</td><td>Zoom in or out</td></tr><tr><td>Ctrl + PgUp, PgDown</td><td>Select previous, next segment</td></tr><tr><td>Ctrl + Home, End</td><td>Select first, last point</td></tr><tr><td>Del</td><td>Delete current point</td></tr></table> | <table><tr><td>Pylkie sleutels</td><td>Skuif kaart links regs, op, af</td></tr><tr><td>Ctrl + links, regs pylkie</td><td>Selekteer vorige of volgende punt</td></tr><tr><td>Ctrl + op, af pylkie</td><td>Vergroot in of uit</td></tr><tr><td>Ctrl + PgUp, PgDown</td><td>Selekteer vorige, volgende segment</td></tr><tr><td>Ctrl + Home, Einde</td><td>Selekteer eerste, laaste punt</td></tr><tr><td>Del</td><td>Verwyder huidige punt</td></tr></table>
dialog.keys.normalmodifier | Modifier text for linux, windows etc | Ctrl | Ctrl
dialog.keys.macmodifier | Modifier text for mac (to replace text in keylist) | Command | Command
dialog.saveconfig.desc | Description on save config dialog | The following settings can be saved to a configuration file : | Die volgende stelligns kan gestoor word na 'n konfigurasie leęr
dialog.saveconfig.prune.trackdirectory | Default directory for data files | Track directory | Baan gids
dialog.saveconfig.prune.photodirectory | Default directory for photos | Photo directory | foto gids
dialog.saveconfig.prune.languagecode |  | Language code (EN) | Taal kode (AF)
dialog.saveconfig.prune.languagefile | Optional additional translation file | Language file | Taal leęr
dialog.saveconfig.prune.gpsdevice | For GPS load/save | GPS device | GPS apparaat
dialog.saveconfig.prune.gpsformat | For GPS load/save | GPS format | GPS formaat
dialog.saveconfig.prune.povrayfont |  | Povray font | Pocray font
dialog.saveconfig.prune.gnuplotpath |  | Path to gnuplot | Pad na gnuplot
dialog.saveconfig.prune.gpsbabelpath |  | Path to gpsbabel | Pad na gpsbabel
dialog.saveconfig.prune.exiftoolpath |  | Path to exiftool | Pad na exitool
dialog.saveconfig.prune.mapsource | Index of currently selected map source | Selected map source | Geselekteerde kaart bron
dialog.saveconfig.prune.mapsourcelist | List of custom map sources | Map sources | Kaarte bronne
dialog.saveconfig.prune.diskcache | Directory where maps are saved | Map cache | Kaart stoorarea
dialog.saveconfig.prune.kmzimagewidth | Size of images in kmz (pixels) | KMZ image size | KMZ beeld groote
dialog.saveconfig.prune.colourscheme | Colour definitions for map background etc | Colour scheme | Kleur skema
dialog.saveconfig.prune.linewidth | Line width used for drawing tracks | Line width | Lyn dikte
dialog.saveconfig.prune.kmltrackcolour |  | KML track colour | KML baan kleur
dialog.saveconfig.prune.autosavesettings | Automatically save settings on exit | Autosave settings | Autostoor stellings
dialog.setpaths.intro | Introductory text for set program paths dialog | If you need to, you can choose the paths to the external applications: | As jy wil, kan jy die paie kies na die eksterne programme:
dialog.setpaths.found | Column heading for yes/no | Path found? | Pad gefind?
dialog.addaltitude.noaltitudes | Warning if altitude offset can't be added | The selected range does not contain altitudes | Die geselekteerde reeks bevat nie hoogtes nie
dialog.addaltitude.desc | Label for edit box | Altitude offset to add | Hoogste verplasing
dialog.lookupsrtm.overwritezeros | Question whether to lookup altitudes for zero-valued altitudes as well as missing ones | Overwrite altitude values of zero? | Oorskryf hoogte waardes van nul?
dialog.setcolours.intro | Description of what to do with the set colours dialog | Click on a colour patch to change the colour | Klik op 'n kleur lap om kleur te verander
dialog.setcolours.background |  | Background | Agtergrond
dialog.setcolours.borders |  | Borders | Grense
dialog.setcolours.lines |  | Lines | Lyne
dialog.setcolours.primary |  | Primary | Primere
dialog.setcolours.secondary |  | Secondary | Sekondere
dialog.setcolours.point |  | Points | Punte
dialog.setcolours.selection |  | Selection | Seleksie
dialog.setcolours.text |  | Text | Teks
dialog.colourchooser.title | Dialog title for choosing a single colour | Choose colour | Kies kleur
dialog.colourchooser.red | Label for red value | Red | Rooi
dialog.colourchooser.green | Label for green value | Green | Groen
dialog.colourchooser.blue | Label for blue value | Blue | Blou
dialog.colourer.intro | Heading for the point colourer section | A point colourer can give track points different colours | 'n punt inkleurder kan baan punte verskillende kleure gee
dialog.colourer.type | Label before type dropdown | Colourer type | Inkleurder tipe
dialog.colourer.type.none | Labels for different colourer types | None | Geen
dialog.colourer.type.byfile | Different colour for each file | By file | Met leęr
dialog.colourer.type.bysegment | Different colour for each track segment | By segment | Volgens segment
dialog.colourer.type.byaltitude | Continuous colour according to altitude | By altitude | Volgens hoogte
dialog.colourer.type.byspeed | Continuous colour according to speed | By speed | Volgens spoed
dialog.colourer.type.byvertspeed | Continuous colour according to vertical speed | By vertical speed | Volgens vertikale spoed
dialog.colourer.type.bygradient | Continuous colour according to gradient | By gradient | Volgens gradiënt
dialog.colourer.type.bydate | Different colour for each date | By date | Volgens datum
dialog.colourer.start | Label above start colour patch | Start colour | Begin kleur
dialog.colourer.end | Label above end colour patch | End colour | Eind kleur
dialog.colourer.maxcolours |  | Maximum number of colours | Maksimum aantal kleure
dialog.setlanguage.firstintro | First label on dialog - use &lt;p&gt; for line break | You can either select one of the included languages,<p>or select a text file to use instead. | 
dialog.setlanguage.secondintro | Second label on dialog | You need to save your settings and then<p>restart GpsPrune to change the language. | 
dialog.setlanguage.language | label before language dropdown | Language | Taal
dialog.setlanguage.languagefile | label before text box for file path | Language file | Taal leęr
dialog.setlanguage.endmessage | Message which pops up after changing language | Now save your settings and restart GpsPrune\nfor the language change to take effect. | 
dialog.setlanguage.endmessagewithautosave | Message which pops up after changing language if settings are autosaved | Please restart GpsPrune for the language change to take effect. | 
dialog.diskcache.save | Checkbox for enabling / disabling disk cache | Save map images to disk | Stoor kaart beelde na skuif
dialog.diskcache.dir | Label for directory entry | Cache directory | Datastoor gds
dialog.diskcache.createdir | Prompt to confirm whether to create specified directory or not | Create directory | Skep gids
dialog.diskcache.nocreate | Message when directory not created (either user pressed cancel or create failed) | Cache directory not created | Datastoor gids nie geskep
dialog.diskcache.cannotwrite | Selected directory is read-only | Map tiles cannot be saved in the selected directory | Kaart teels kon nie gestoor word in die geselekteerde gids
dialog.diskcache.table.path | Table column heading for relative file path | Path | Pad
dialog.diskcache.table.usedby | Which backgrounds use this tileset | Used by | Gebruik deur
dialog.diskcache.table.zoom | Zoom range covered | Zoom | Verhoog
dialog.diskcache.table.tiles | Number of tiles found | Tiles | Teëls
dialog.diskcache.table.megabytes | Number of megabytes used by all tiles | Megabytes | Megagrepe
dialog.diskcache.tileset | Path to tileset | Tileset | Teëlstel
dialog.diskcache.tileset.multiple | shown when more than one tileset selected, instead of all the paths | multiple | verskeie
dialog.diskcache.deleteold | radio button to delete files by age | Delete old tiles | Verwyder ou teëls
dialog.diskcache.maximumage | label for the maximum age of tiles before they're deleted | Maximum age (days) | Maksimum ouderdom (dae)
dialog.diskcache.deleteall | radio button to delete all files in the tileset | Delete all tiles | Verwyder alle teëls
dialog.diskcache.deleted | Makes the message "Deleted &lt;number&gt; of tiles from the cache" | Deleted %d files from the cache | %d leers verwyder vanuit datastoor
dialog.deletefieldvalues.intro | Label at the top of the delete field values dialog | Select the field to delete for the current range | Selekteer die veld om te verwyder vanuit die hudige reeks
dialog.deletefieldvalues.nofields | Message when there are no fields to delete | There are no fields to delete for this range | Daar is geen velde om te verwyder vir hierdie reeks nie
dialog.setlinewidth.text | Dialog for setting line thickness in pixels | Enter the thickness of lines to draw for the tracks (1-4) | Sleutel die dikte van die lyne om te teken vir die bane (1-4)
dialog.downloadosm.desc | Description at top of download OSM data dialog | Confirm to download the raw OSM data for the specified area: | Bevestig die aflaai van rou OSM data vir die geslekteerde area:
dialog.searchwikipedianames.search | Search box | Search for: | Soek vir:
dialog.weather.location | Where the weather forecast is for | Location | Plek 
dialog.weather.update | When the forecast was last updated | Forecast updated | Voorspelling opgedateer
dialog.weather.sunrise | Time of sunrise | Sunrise | Sonsopkomssdag
dialog.weather.sunset | Time of sunset | Sunset | Sonsondergang
dialog.weather.temperatureunits | Units for the temperatures (Celsius or Fahrenheit) | Temperatures | Temperature
dialog.weather.currentforecast | Get the current weather | Current weather | Huidige weer
dialog.weather.dailyforecast | Get one forecast for each day | Daily forecast | Daaglikse voospelling
dialog.weather.3hourlyforecast | Get a forecast for each 3 hours | Three-hourly forecast | Drie-uurlikse voorspelling
dialog.weather.day.now |  | Current weather | Huidige weer
dialog.weather.day.today | Days for showing in the weather table | Today | Vandag
dialog.weather.day.tomorrow |  | Tomorrow | More
dialog.weather.day.monday |  | Monday | Maandag
dialog.weather.day.tuesday |  | Tuesday | Dinsdag
dialog.weather.day.wednesday |  | Wednesday | Woensdag
dialog.weather.day.thursday |  | Thursday | Donderdag
dialog.weather.day.friday |  | Friday | Vrydag
dialog.weather.day.saturday |  | Saturday | Saturdag
dialog.weather.day.sunday |  | Sunday | Sondag
dialog.weather.wind |  | Wind | Wind
dialog.weather.temp | Maybe should be shortened to fit in the forecast table | Temp | Temp
dialog.weather.humidity |  | Humidity | Himuditeit
dialog.weather.creditnotice | Thanks to openweathermap.org | This data is made available by openweathermap.org. Their website has more details. | Heirdie data is beskikbaar gestel deur openweather.org. Hulle webwerf het meer besonderhede
dialog.deletebydate.onlyonedate | Can't delete by date if there's only one date | The points were all recorded on the same date. | Die punte was almal opgeneem op dieselfde datum
dialog.deletebydate.intro | Explanation at top of delete by date dialog | For each date in the track, you can choose to delete or keep the points | Vir elke datum in tdie baan,
dialog.deletebydate.nodate | Table entry for points without a date | No timestamp | Geen tydstempel
dialog.deletebydate.column.keep | Column heading in table - radio button for DON'T delete | Keep | Hou
dialog.deletebydate.column.delete | Same but for radio button to DELETE these points (see button.delete) | Delete | Verwyder
dialog.setaltitudetolerance.text.metres | Label to describe input parameter to tolerance (using metres for altitudes) | Limit (in metres) below which small climbs and descents will be ignored | 
dialog.setaltitudetolerance.text.feet | Label to describe input parameter to tolerance (using feet for altitudes) | Limit (in feet) below which small climbs and descents will be ignored | 

## 3d window ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
dialog.3d.title | Window title | GpsPrune Three-d view | Gpsprune Three-d vertoon
dialog.3d.altitudefactor |  | Altitude exaggeration factor | Hoogte oordrywings faktor

## Confirm messages ##
*These are displayed as confirmation in the status bar*

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
confirm.loadfile | Confirm data loaded from file | Data loaded from file | Data gelaai vanaf leer
confirm.save.ok1 |  | Successfully saved | Sukselvol gestoor
confirm.save.ok2 | These two make the message "Successfully saved &lt;number&gt; points to file &lt;filename&gt;" | points to file | punte na leer
confirm.deletepoint.single | message for single point deleted | data point was removed | data punt was verwyder
confirm.deletepoint.multi | message for more than one point deleted | data points were removed | data punte was verwyder
confirm.point.edit |  | point edited | punter geredigeer
confirm.mergetracksegments |  | Track segments merged | Baan segmente saamgevoeg
confirm.reverserange |  | Range reversed | Reeks omgekeer
confirm.addtimeoffset |  | Time offset added | Tyd verplasing bygevoeg
confirm.addaltitudeoffset |  | Altitude offset added | Hoogte verplasing bygevoeg
confirm.rearrangewaypoints |  | Waypoints rearranged | Bakens ge-herrangskik
confirm.rearrangephotos |  | Photos rearranged | Fotos ge-herrangskik
confirm.splitsegments | The %d is replaced by the number of splits | %d segment splits were made | %d segment verdelings is gemaak
confirm.sewsegments | The %d is replaced by the number of joins | %d segment joins were made | %d segment las is gemaak
confirm.cutandmove |  | Selection moved | Seleksie geskuif
confirm.interpolate |  | Points added | Punte bygevoeg
confirm.convertnamestotimes |  | Waypoint names converted | Baken name aangepas
confirm.saveexif.ok | Makes the message to confirm the number of photos saved | Saved %d photo files | %d fotos gestoor
confirm.undo.single |  | operation undone | operasie ongedaan
confirm.undo.multi | These two make the message "&lt;number&gt; operations undone" to confirm that they were undone successfully | operations undone | operasies ongedaan
confirm.jpegload.single |  | photo was added | foto was bygevoeg
confirm.jpegload.multi |  | photos were added | fotos was bygevoeg
confirm.media.connect | Either a photo or an audio was connected | media connected | media gekoppel
confirm.photo.disconnect |  | photo disconnected | foto ontkoppel
confirm.audio.disconnect |  | audio disconnected | klank ontkoppel
confirm.media.removed | &lt;filename&gt; removed | removed | verwyder
confirm.correlatephotos.single | Confirm one photo correlation | photo was correlated | foto was gekorreleer
confirm.correlatephotos.multi | Confirm more than one photo correlation | photos were correlated | fotos was gekorreleer
confirm.createpoint |  | point created | foto geskep
confirm.rotatephoto | current photo was rotated (by 90 degrees) | photo rotated | foto geroteer
confirm.running | Displayed when an export is currently running (and may take some time) | Running ... | Besig om te loop...
confirm.lookupsrtm | Makes the message "Found &lt;number&gt; altitude values" for SRTM lookup function | Found %d altitude values | %d hoogte waardes gevind
confirm.downloadsrtm | Confirms download of SRTM files | Downloaded %d files to the cache | %d leers afgelaai na data stoor
confirm.downloadsrtm.1 | Confirms download of just 1 SRTM file | Downloaded %d file to the cache | %d leer afgelaai na data stoor
confirm.downloadsrtm.none | All the necessary tiles were already present | No files downloaded, they were already in the cache | Geen leers afgelaai, hulle was alreeds in the data stoor
confirm.deletefieldvalues |  | Field values deleted | Veld waardes verwyder
confirm.audioload |  | Audio files added | Klank leers bygevoeg
confirm.correlateaudios.single | Confirm one audio clip correlated | audio was correlated | klankgreep was gekorreleer
confirm.correlateaudios.multi | Confirm more than one audio clip correlated | audios were correlated | klankgrepe was gekorreleer

## Tips, shown just once when appropriate ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
tip.title |  | Tip | Wenk
tip.useamapcache | Shown when many maps are shown but cache is off | By setting up a disk cache (Settings -> Save maps to disk)\nyou can speed up the display and reduce network traffic. | 
tip.learntimeparams | Shown when estimate is called but no learn has been done | The results will be more accurate if you use\nTrack -> Learn time estimation parameters\non your recorded tracks. | 
tip.downloadsrtm | Shown when many SRTM lookups have been made with online calls | You can speed this up by calling\nOnline -> Download SRTM tiles\nto save the data in your map cache. | 
tip.usesrtmfor3d | Shown when a 3d view is requested but track doesn't have altitudes | This track doesn't have altitudes.\nYou can use the SRTM functions to get approximate\naltitudes for the 3d view. | 
tip.manuallycorrelateone | Tip to recommend that at least one photo/audio is manually connected | By manually connecting at least one item, the time offset can be calculated for you. | Deur een item te verbind, kan die tyd afset bereken word vir jou.

## Buttons ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
button.ok |  | OK | OK
button.back |  | Back | Terug
button.next |  | Next | Volgende
button.finish |  | Finish | Eindig
button.cancel |  | Cancel | Kanselleer
button.overwrite |  | Overwrite | Oorskryf
button.moveup | Move the selected field up one position in the list | Move up | Skuif op
button.movedown | Move the selected field down one position in the list | Move down | Skuif af
button.edit |  | Edit | Rediggeer
button.exit |  | Exit | Verlaat
button.close |  | Close | Toemaak
button.continue |  | Continue | Voortgaan
button.yes |  | Yes | Ja
button.no |  | No | Nee
button.yestoall |  | Yes to all | Ja aan almal
button.notoall |  | No to all | Nee aan almal
button.always | Yes, and don't ask me again | Always | Altyd
button.select |  | Select | Selekteer
button.selectall |  | Select all | Selekteer almal
button.selectnone |  | Select none | Selekter geen
button.preview |  | Preview | Voorskou
button.load |  | Load | Laai
button.upload | Send data from GpsPrune to a website (like gpsies) | Upload | Oplaai
button.guessfields | Guess which fields go where | Guess fields | Raai velde
button.showwebpage | Launch a browser to show the given web page | Show webpage | Wys webblad
button.check | Check the values in the dialog | Check | Kontroleer
button.resettodefaults |  | Reset to defaults | Herstel na verstek waarde
button.browse | Open a file selection dialog to choose a file | Browse... | Blaai...
button.addnew | Add a new item, for example a new map source | Add new | Voeg nuwe
button.delete | Delete currently selected item | Delete | Verwyder
button.manage | Open dialog for managing the item, eg map cache | Manage | Bestuur
button.combine | Used to combine the calculated parameters | Combine | Kombineer

## File types ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
filetype.txt | Text files | TXT files | TXT leers
filetype.jpeg |  | JPG files |  JPG leers
filetype.kmlkmz | KML and KMZ | KML, KMZ files | KML, KMZ leers
filetype.kml |  | KML files | KML leers
filetype.kmz |  | KMZ files | KMZ leers
filetype.gpx |  | GPX files | GPX leers
filetype.pov |  | POV files | POV leers
filetype.svg |  | SVG files | SVG leers
filetype.png |  | PNG files | PNG leers
filetype.audio |  | MP3, OGG, WAV files | MP3, OGG, WAV leers

## Display components ##
*These are all for the side panels showing point/range details*

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
display.nodata |  | No data loaded | 
display.noaltitudes | Altitude chart can't be shown | Track data does not include altitudes | Baan data sluit geen hoogte data in
display.notimestamps | Speed chart can't be shown | Track data does not include timestamps | Baan data sluit geen tydstempels in
display.novalues | Chart for another field can't be shown | Track data does not include values for this field | Baan data sluit geen waardes in vir hierdie veld
details.trackdetails |  | Track details | Baan besonderhede
details.notrack |  | No track loaded | Geen baan gelaai
details.track.points |  | Points | Punte
details.track.file |  | File | Leer
details.track.numfiles | Number of files loaded | Number of files | Aantal leers
details.pointdetails |  | Point details | Punt besonderhede
details.index.selected |  | Index | Indeks
details.index.of | These two make the message "Index 32 of 425" if there are 425 points and number 32 is selected | of | van
details.nopointselection |  | No point selected | Geen punt geselekteer
details.photofile |  | Photo file | Foto leer
details.norangeselection |  | No range selected | Geen reeks geselekteer
details.rangedetails |  | Range details | Reeks besonderhede
details.range.selected |  | Selected | Geselekteer
details.range.to | These two make the message "Selected 128 to 181" if the current range goes from point number 128 to number 181 | to | na
details.altitude.to | Makes the message "139m to 1128m" if that is the altitude range of the selection | to | na
details.range.climb | The total amount of upwards climb during the selection | Climb | Klim
details.range.descent | The total amount of downwards descent during the selection | Descent | Val
details.coordformat |  | Coordinate format | Koordinaat formaat
details.distanceunits |  | Distance units | Afstand eenhede
display.range.time.secs |  | s | s
display.range.time.mins |  | m | m
display.range.time.hours |  | h | u
display.range.time.days |  | d | d
details.range.avespeed |  | Ave speed | Gem spoed
details.range.maxspeed |  | Maximum speed | Maksimum spoed
details.range.numsegments |  | Number of segments | Aantal segmente
details.range.pace | Time for 1 km or 1 mile | Pace | Pas
details.range.gradient | Total altitude difference as percentage of total distance | Gradient | Gradient
details.lists.waypoints | Headings above list boxes | Waypoints | Bakens
details.lists.photos |  | Photos | Fotos
details.lists.audio |  | Audio | Klank
details.photodetails |  | Photo details | Foto besonderhede
details.nophoto |  | No photo selected | Geen fotos geselekteer
details.photo.loading | Shown when thumbnail loading | Loading | Besig om te laai
details.photo.bearing | Direction in which photo was taken (degrees) | Bearing | 
details.media.connected | Is the photo / audio associated with a point | Connected | Gekoppel
details.media.fullpath | Full filename or URL of photo/audio | Full path | Vol pad
details.audiodetails |  | Audio details | Klank besonderhede
details.noaudio |  | No audio clip selected | Geen klankgreep geslekteer
details.audio.file |  | Audio file | Klank leer
details.audio.playing | Shown in the progress bar when an audio clip is currently playing | playing... | Besig om te speel...
map.overzoom | Message shown when zoom is too high for maps | No maps available at this zoom level | Geen kaart beskikbaar by hierdie vehogings vlak

## Field names ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
fieldname.latitude |  | Latitude | breedtegraad
fieldname.longitude |  | Longitude | lengtegraad
fieldname.altitude |  | Altitude | Hoogte
fieldname.timestamp | Timestamp of single point | Time | Tyd
fieldname.time | Time as an axis variable | Time | Tyd
fieldname.date | Just the date, without time of day | Date | Datum
fieldname.waypointname |  | Name | Naam
fieldname.waypointtype |  | Type | Tipe
fieldname.newsegment |  | Segment | Segment
fieldname.custom |  | Custom | Pasmaak
fieldname.prefix |  | Field | Veld
fieldname.distance |  | Distance | Afstand
fieldname.duration |  | Duration | Tydperk
fieldname.speed |  | Speed | Spoed
fieldname.verticalspeed |  | Vertical speed | Vertikale spoed
fieldname.description |  | Description | Beskrywing

## Measurement units ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
units.original | use the original format | Original | Oorspronklik
units.default | use the default format | Default | Bestek
units.metres |  | Metres | Meters
units.metres.short |  | m | m
units.feet |  | Feet | Voet
units.feet.short |  | ft | ft
units.kilometres |  | Kilometres | Kilo meters
units.kilometres.short |  | km | km
units.kilometresperhour |  | km per hour | km per uur
units.kilometresperhour.short |  | km/h | km/h
units.miles |  | Miles | Myl
units.miles.short |  | mi | mi
units.milesperhour |  | miles per hour | myl per uur
units.milesperhour.short |  | mph | mph
units.nauticalmiles |  | Nautical miles | See myle
units.nauticalmiles.short |  | N.m. | N.m.
units.nauticalmilesperhour.short | knots | kts | kts
units.metrespersec | for vertical speed | metres per second | meters per sekond
units.metrespersec.short | for vertical speed | m/s | m/s
units.feetpersec | for vertical speed | feet per second | voet per sekond 
units.feetpersec.short | for vertical speed | ft/s | ft/s
units.hours |  | hours | ure
units.minutes |  | minutes | minute
units.seconds |  | seconds | sekonde
units.degminsec |  | Deg-min-sec | Deg-min-sec
units.degmin |  | Deg-min | Deg-min
units.deg |  | Degrees | Grade
units.iso8601 | long timestamp format | ISO 8601 | ISO 8601
units.degreescelsius | for temperatures in Celsius | Celsius | Celcius
units.degreescelsius.short |  | °C | °C
units.degreesfahrenheit | for temperatures in Fahrenheit | Fahrenheit | Fahrenheit
units.degreesfahrenheit.short |  | °F | °F

## How to combine conditions, such as filters ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
logic.and | both must be true | and | en
logic.or | either can be true | or | of

## External urls and services ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
url.googlemaps | hostname for google maps | maps.google.co.uk | maps.google.co.uk
wikipedia.lang | language code for wikipedia | en | en
openweathermap.lang | language code for openweathermap.org | en | en
webservice.peakfinder |  | Open Peakfinder.org | Open Peakfinder.org
webservice.geohack |  | Open Geohack page | Open Geohack bladsy
webservice.panoramio |  | Open Panoramio map | Open Panoramio kaart
webservice.opencachingcom |  | Open Opencaching.com | Open Opencaching.com
webservice.opencachingcom.lang | Language supported by opencaching.com | en | en


## Cardinals for 3d plots ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
cardinal.n |  | N | N
cardinal.s |  | S | S
cardinal.e |  | E | O
cardinal.w |  | W | W

## Undo operations ##
*These will be displayed in the undo list after you've performed the operation, to tell you what you did*

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
undo.load | load data | load data | laai data
undo.loadphotos |  | load photos | laai fotos
undo.loadaudios |  | load audio clips | laai klankgrepe
undo.editpoint |  | edit point | eind punt
undo.deletepoint |  | delete point | verwyder punt
undo.removephoto |  | remove photo | verwyder foto
undo.removeaudio |  | remove audio clip | verwyder klankgreep
undo.deleterange |  | delete range | verwyder reeks
undo.croptrack |  | crop track | Kleinknip baan
undo.deletemarked |  | delete points | verwyder punte
undo.insert |  | insert points | voeg punte by
undo.reverse |  | reverse range | keer baan om
undo.mergetracksegments |  | merge track segments | smelt baan segmente
undo.splitsegments |  | split track segments | verdeel baan segmente
undo.sewsegments |  | sew track segments | naai baan segmente
undo.addtimeoffset |  | add time offset | voeg tyd verplasing
undo.addaltitudeoffset |  | add altitude offset | voeg hoogte afset
undo.rearrangewaypoints |  | rearrange waypoints | herrangskik
undo.cutandmove |  | move section | beweeg seksie
undo.connect |  | connect | verbind
undo.disconnect |  | disconnect | afsluit
undo.correlatephotos |  | correlate photos | korreleer fotos
undo.rearrangephotos |  | rearrange photos | herrangskik fotos
undo.createpoint |  | create point | skep punt
undo.rotatephoto |  | rotate photo | roteer foto
undo.convertnamestotimes |  | convert names to times | skakel name na tye
undo.lookupsrtm |  | lookup altitudes from SRTM | soek hoogtes vanaf SRTM
undo.deletefieldvalues |  | delete field values | verwyder veld waardes
undo.correlateaudios |  | correlate audios | korreleer klankgrepe

## Error messages ##

**Key** | **Description** | **English** | **Afrikaans**
------- | --------------- | ----------- | ---------
error.save.dialogtitle |  | Error saving data | Fout om data te stoor
error.save.nodata |  | No data to save | Geen data om te stoor
error.save.failed |  | Failed to save the data to file | Stoor van data na lęer het misluk
error.saveexif.filenotfound |  | Failed to find photo file | Find van foto het misluk
error.saveexif.cannotoverwrite1 |  | Photo file | Foto leer
error.saveexif.cannotoverwrite2 | These two together make the message when a photo is read-only but overwrite has been selected | is read-only and can't be overwritten. Write to copy? | is lees-alleen en kan nie oorskruif word nie. Skyf na kopie?
error.saveexif.failed | Makes the message when (number) of images failed to be saved | Failed to save %d of the images | Stoor het misluk %d van die beelde
error.saveexif.forced | Makes the warning when (number) of images were forced | %d of the images required forcing | %d van die beelde vereis vorsering
error.load.dialogtitle |  | Error loading data | Fout met laai van data
error.load.noread | Error message for when the file can't be read at all | Cannot read file | Kan nie leer lees
error.load.nopoints | Error message shown when no points found | No coordinate information found in the file | Geen koordinaat informasie gevind in the leer
error.load.unknownxml | XML file isn't kml or gpx | Unrecognised xml format: | Onherkenbare xml formaat:
error.load.noxmlinzip | Zip file or Kmz file opened but no xml file found inside | No xml file found inside zip file | Geen xml leer gevind binne zip leer
error.load.othererror |  | Error reading file: | 
error.jpegload.dialogtitle |  | Error loading photos | Fout met fotos laai
error.jpegload.nofilesfound | No files found | No files found | Geen leers gevind
error.jpegload.nojpegsfound | No jpeg files found | No jpeg files found | Geen jpeg leers gevind
error.jpegload.nogpsfound | No jpeg files with gps data in exif | No GPS information found | Geen GPS informasie gevind
error.jpegload.exifreadfailed | Either the jar wasn't built correctly, or libmetadata-extractor-java needs to be in the classpath when run | Failed to read Exif information. No Exif information can be read\nwithout either an internal or external library. | 
error.audioload.nofilesfound |  | No audio clips found | 
error.gpsload.unknown | Something went wrong with gps load but no error message given | Unknown error | Onbekende fout
error.undofailed.title |  | Undo failed | Herroep fout
error.undofailed.text |  | Failed to undo operation | Fout met herroep operasie
error.function.noop.title |  | Function had no effect | Funksie het geen effek gehad
error.rearrange.noop | Update: Now just POINTS, not waypoints | Rearranging points had no effect | Herrangskikking van punte het geen effek gehad
error.function.notavailable.title |  | Function not available | Funksie nie beskikbaar
error.function.nojava3d |  | This function requires the Java3d library. | Hierdie funksie het Java3d biblioteek nodig
error.3d | Error with 3d display | An error occurred with the 3d display | 'n fout het gebeur met die 3d vertoon
error.readme.notfound |  | Readme file not found | Readme leer nie gevind
error.osmimage.dialogtitle |  | Error loading map images | Fout met laai van kaart beelde
error.osmimage.failed | Probably there is no network connection | Failed to load map images. Please check internet connection. | Milsukking met die laai van kaart beelde.Toets internet conneksie
error.language.wrongfile | Probably just chose the wrong file | The selected file doesn't appear to be a language file for GpsPrune | Die geselekteerde leer lyk nie soos 'n taal leer vir GpsPrune
error.convertnamestotimes.nonames | Either no waypoint names were found, or they couldn't be converted | No names could be converted into times | Geen name kon in tye omgeskakel word
error.lookupsrtm.nonefound | Either no tiles found for this area, or data contains voids | No altitude values available for these points | Geen hoogte waardes beskikbaar vir die punte
error.lookupsrtm.nonerequired | No points found without altitudes, so nothing to lookup | All points already have altitudes, so there's nothing to lookup | Al die punte het klaar hoogtes, so daar is niks meer om te soek
error.gpsies.uploadnotok | The gpsies server didn't return an OK (followed by : and message) | The gpsies server returned the message | Die gpsies server het die boodskap terug gestuur
error.gpsies.uploadfailed | The call never got to the gpsies server because of an error (followed by : and error message) | The upload failed with the error | Die oplaai het misluk met die volgende fout boodskap
error.showphoto.failed | Popup launched but photo can't be loaded (maybe moved?) | Failed to load photo | Foto het nie gelaai nie
error.playaudiofailed | Java couldn't play it and the Desktop call failed too | Failed to play audio clip |  Klankgreep het nie gespeel nie
error.cache.notthere | The 'manage' button was clicked but the directory is no longer there | The tile cache directory was not found | Die teël stoorarea gids kon nie opgespoor word nie
error.cache.empty | The tile cache directory is there but there are no subdirectories in it | The tile cache directory is empty | Die teël stoorarea gids is leeg
error.cache.cannotdelete | Either we haven't got permission to delete or none are too old | No tiles could be deleted | Geen teëls kon verwyder word
error.learnestimationparams.failed | Error when learning the estimation parameters failed - the track is too short or doesn't have enough data | Cannot learn the parameters from this track.\nTry loading more tracks. | 
error.tracksplit.nosplit | The split function couldn't find any places to split the track | The track could not be split | Die baan kon nie verdeel word
error.downloadsrtm.nocache | Either there isn't a cache configured, or it doesn't have write permissions | The files could not be saved.\nPlease check the disk cache. | Die leers kon nie gestoor word.\nGaan asseblief die skyf stoorarea na
error.sewsegments.nothingdone | Sew segment operation couldn't do anything (also show number of segments) | No segments could be sewn together.\nThere are now %d segments in the track. | Geen segmente kon aanmekaar genaai word.\nDaar is nou %d segmente in the baan
